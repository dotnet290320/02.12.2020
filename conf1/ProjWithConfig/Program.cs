﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MultiDB
{
    interface IPoco { }
    class Ticket : IPoco
    {
        public Ticket()
        {

        }
        public Int64 Id { get; set; }
        public string MovieName { get; set; }
        public DateTime TimeStamp { get; set; }
        public double Price { get; set; }
        public int Hall { get; set; }

        public override bool Equals(object obj)
        {
            return this.Id == ((Ticket)obj).Id;
        }

        public override int GetHashCode()
        {
            return (int)Id;
        }
        public static bool operator ==(Ticket ticket1, Ticket ticket2)
        {
            return ticket1.Id == ticket2.Id;
        }
        public static bool operator !=(Ticket ticket1, Ticket ticket2)
        {
            return !(ticket1 == ticket2);
        }
        public override string ToString()
        {
            return $"{Newtonsoft.Json.JsonConvert.SerializeObject(this)}";
        }

    }
    class Program
    {
        private static readonly log4net.ILog my_logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static MultiDbAppConfig m_config;

        static void PrintAllTickets(SqlConnection connection)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM TICKETS", connection);
            cmd.CommandType = CommandType.Text;

            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.Default);
            // cmd.ExecuteNonQuery

            //List<Object> list = new List<object>();
            while (reader.Read() == true)
            {
                Console.WriteLine($" {reader["ID"]} {reader["MOVIENAME"]} {reader["TIMESTAMP"]} {reader["PRICE"]} {reader["HALL"]}");
                //var e = new
                //{
                //    Id = reader["ID"],
                //    firaName = reader["FIRSTNAME"]
                //};
                //list.Add(e);
            }

            // add INSERT QUERY
        }

        private static SqlConnection GetOpenConnection(string conn)
        {
            SqlConnection result = new SqlConnection(conn);
            result.Open();
            return result;
        }

        private static void InsertNewTicket(Ticket ticket, SqlConnection connection)
        {
            if (m_config.AllowDBWrite)
            {

                string insert_query = "INSERT INTO TICKETS (MOVIENAME, TIMESTAMP, PRICE, HALL) " +
                            $" VALUES ('{ticket.MovieName}', '{ticket.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss")}'," +
                            $" {ticket.Price}, {ticket.Hall}); SELECT SCOPE_IDENTITY();";
                try
                {
                    SqlCommand cmd = new SqlCommand(insert_query,
                            connection);
                    cmd.CommandType = CommandType.Text;
                    Decimal result = Convert.ToDecimal(cmd.ExecuteScalar());
                    my_logger.Info($"New ticket record {ticket.MovieName} was inserted with ID {result}");
                    ticket.Id = (long)result;
                }
                catch (Exception ex)
                {
                    my_logger.Error($"Failed to insert new ticket into data base. Error : {ex}");
                    my_logger.Error($"InsertNewTicket: [{insert_query}]");
                }
            }
            else
            {
                my_logger.Info("Tried to write into Db while in read-pnly mode");
                Console.WriteLine($"Not allow to write into DB. check config");
            }
        }

        static List<Ticket> GetAllTickets(SqlConnection connection)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM TICKETS", connection);
            cmd.CommandType = CommandType.Text;

            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.Default);

            List<Ticket> list = new List<Ticket>();

            while (reader.Read() == true)
            {
                list.Add(
                    new Ticket
                    {
                        Id = Convert.ToInt64(reader["Id"]),
                        MovieName = reader["MOVIENAME"].ToString(),
                        TimeStamp = Convert.ToDateTime(reader["TIMESTAMP"]),
                        Price = Convert.ToDouble(reader["PRICE"]),
                        Hall = Convert.ToInt32(reader["HALL"])
                    });
            }

            return list;
        }

        static Ticket GetTicketById(SqlConnection connection, long id)
        {
            //try catch + log.error if needed
            return null;
        }

        static bool UpdateTickets(SqlConnection connection, Ticket ticket)
        {
            //try catch + log.error if needed
            // return true if row affected == 1 else return false
            // update ... where ticket.Id = ...
            return false;
        }
        static bool DeleteTicketById(SqlConnection connection, Ticket ticket)
        {
            //try catch + log.error if needed
            // return true if row affected == 1 else return false
            // delete ... where ticket.Id = ...
            return false;
        }

        static void Main(string[] args)
        {

            my_logger.Info("******************** System startup");



            m_config = new MultiDbAppConfig();
            m_config.Init();

            //string m_conn = @"Data Source=.;Initial Catalog=Many;Integrated Security=True";
            string m_conn = m_config.ConnectionString;

            Console.WriteLine($"-- Hello App {m_config.AppName}");

            //PrintAllTickets(GetOpenConnection(m_conn));

            Ticket ticket_to_bad_boys = new Ticket
            {
                MovieName = "Iron man",
                Hall = 2,
                Price = 42.1,
                TimeStamp = DateTime.Now
            };

            InsertNewTicket(ticket_to_bad_boys, GetOpenConnection(m_conn));

            //PrintAllTickets(GetOpenConnection(m_conn));

            List<Ticket> tickets = GetAllTickets(GetOpenConnection(m_conn));
            tickets.ForEach(_ => Console.WriteLine(_));

            my_logger.Info("******************** System shutdown");

            //Console.ReadLine();
        }
    }
}
